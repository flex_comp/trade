package trade

import (
	"encoding/json"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/conf"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
	"io/ioutil"
	"os"
	"sync"
	"time"
)

var (
	trades *Trades
)

func init() {
	trades = &Trades{
		lib:     make(map[string]Trade),
		watcher: make(map[string]map[uint64]chan *Order),
	}

	_ = comp.RegComp(trades)
}

type Trades struct {
	lib map[string]Trade

	watcher map[string]map[uint64]chan *Order

	counter uint64
	mtx     sync.RWMutex
}

func (t *Trades) Init(map[string]interface{}, ...interface{}) error {
	path := util.ToString(conf.Get("exchange"))
	if len(path) == 0 {
		return ErrNotFoundConf
	}

	srcFile, e := os.OpenFile(path, os.O_RDWR, 0666)
	if e != nil {
		return e
	}

	defer srcFile.Close()
	raw, e := ioutil.ReadAll(srcFile)
	if e != nil {
		return e
	}

	config := make(map[string][]interface{})
	e = json.Unmarshal(raw, &config)
	if e != nil {
		return e
	}

	<-time.After(time.Second * 1)
	for name, args := range config {
		impl, ok := t.lib[name]
		if !ok {
			log.Warn("未知的交易所:", name)
			continue
		}

		ch := impl.Run(args...)
		name := name
		go func() {
			for o := range ch {
				o.Exchange = name
				t.readOrder(o)
			}
		}()
	}

	return nil
}

func (t *Trades) readOrder(o *Order) {
	t.mtx.RLock()
	defer t.mtx.RUnlock()

	token := o.Exchange + o.Symbol
	l, ok := t.watcher[token]
	if !ok {
		return
	}

	for _, orders := range l {
		orders <- o
	}
}

func (t *Trades) Start(...interface{}) error {
	return nil
}

func (t *Trades) UnInit() {

}

func (t *Trades) Name() string {
	return "Trades"
}

func Reg(trade Trade) {
	trades.Reg(trade)
}

func (t *Trades) Reg(trade Trade) {
	t.lib[trade.Name()] = trade
}

func UnReg(trade Trade) {
	trades.UnReg(trade)
}

func (t *Trades) UnReg(trade Trade) {
	delete(t.lib, trade.Name())
}

func WatchOrder(exchange, symbol string) (chan *Order, uint64, error) {
	return trades.WatchOrder(exchange, symbol)
}

func (t *Trades) WatchOrder(exchange, symbol string) (chan *Order, uint64, error) {
	t.mtx.Lock()
	defer t.mtx.Unlock()

	token := exchange + symbol
	m, ok := t.watcher[token]
	if !ok {
		m = make(map[uint64]chan *Order)
		t.watcher[token] = m
	}

	ch := make(chan *Order, 1)
	t.counter++
	m[t.counter] = ch

	return ch, t.counter, nil
}

func UnWatchOrder(exchange, symbol string, id uint64) {
	trades.UnWatchOrder(exchange, symbol, id)
}

func (t *Trades) UnWatchOrder(exchange, symbol string, id uint64) {
	t.mtx.Lock()
	defer t.mtx.Unlock()

	token := exchange + symbol
	m, ok := t.watcher[token]
	if !ok {
		return
	}

	delete(m, id)
}

func PutOrder(o *Order) (*Order, error) {
	return trades.PutOrder(o)
}

func (t *Trades) PutOrder(o *Order) (*Order, error) {
	impl, ok := t.lib[o.Exchange]
	if !ok {
		return nil, ErrNotFoundExchange
	}

	return impl.NewOrder(o)
}

func CancelOrder(exchange, id string) error {
	return trades.CancelOrder(exchange, id)
}

func (t *Trades) CancelOrder(exchange, id string) error {
	impl, ok := t.lib[exchange]
	if !ok {
		return ErrNotFoundExchange
	}

	impl.CancelOrder(id)
	return nil
}

func Balance(exchange string) (float64, error) {
	return trades.Balance(exchange)
}

func (t *Trades) Balance(exchange string) (float64, error) {
	impl, ok := t.lib[exchange]
	if !ok {
		return 0, ErrNotFoundExchange
	}

	return impl.Balance(), nil
}

func Orders(exchange, symbol string) ([]*Order, error) {
	return trades.Orders(exchange, symbol)
}

func (t *Trades) Orders(exchange, symbol string) ([]*Order, error) {
	impl, ok := t.lib[exchange]
	if !ok {
		return nil, ErrNotFoundExchange
	}

	return impl.Orders(symbol), nil
}

func Positions(exchange, symbol string) ([]*Position, error) {
	return trades.Positions(exchange, symbol)
}

func (t *Trades) Positions(exchange, symbol string) ([]*Position, error) {
	impl, ok := t.lib[exchange]
	if !ok {
		return nil, ErrNotFoundExchange
	}

	return impl.Positions(symbol), nil
}

func Account(exchange string) (string, error) {
	return trades.Account(exchange)
}

func (t *Trades) Account(exchange string) (string, error) {
	impl, ok := t.lib[exchange]
	if !ok {
		return "", ErrNotFoundExchange
	}

	return impl.Account(), nil
}

func Sync(exchange, symbol string) error {
	return trades.Sync(exchange, symbol)
}

func (t *Trades) Sync(exchange, symbol string) error {
	impl, ok := t.lib[exchange]
	if !ok {
		return ErrNotFoundExchange
	}

	impl.Sync(symbol)
	return nil
}
