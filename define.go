package trade

import (
	"errors"
	"time"
)

type Trade interface {
	Name() string
	Run(...interface{}) chan *Order
	NewOrder(*Order) (*Order, error)
	CancelOrder(id string)

	Sync(symbol string)
	Account() string
	Orders(symbol string) []*Order
	Positions(symbol string) []*Position
	Balance() float64
}

type OrderType uint8

const (
	Market OrderType = iota
	Limit
)

type PositionSide uint8

const (
	Long PositionSide = iota
	Short
)

type Position struct {
	Exchange string
	Symbol   string
	Side     PositionSide
	Price    float64
	Quantity float64
	Raw      map[string]interface{}
}

type Order struct {
	Id       string
	Exchange string
	Symbol   string
	Ty       OrderType
	Side     PositionSide
	Price    float64
	Quantity float64
	Time     time.Time
	Raw      map[string]interface{}
}

func NewOrder(exchange, symbol string, ty OrderType, side PositionSide, price, quantity float64) *Order {
	return &Order{
		Exchange: exchange,
		Symbol:   symbol,
		Ty:       ty,
		Side:     side,
		Price:    price,
		Quantity: quantity,
	}
}

var (
	ErrNotFoundExchange = errors.New("未找到交易所")
	ErrSubRepeated      = errors.New("指定 交易所,合约 重复订阅")
	ErrNotFoundConf     = errors.New("未找到交易所配置")
)
