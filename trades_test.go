package trade

import (
	"fmt"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/log"
	"math/rand"
	"testing"
	"time"
)

type ExchangeTrade struct {
	ch chan *Order
}

func (e *ExchangeTrade) Sync(symbol string) {

}

func (e *ExchangeTrade) Account() string {
	return "acc-test"
}

func (e *ExchangeTrade) CancelOrder(id string) {

}

func (e *ExchangeTrade) Orders(symbol string) []*Order {
	return nil
}

func (e *ExchangeTrade) Positions(symbol string) []*Position {
	return nil
}

func (e *ExchangeTrade) Balance() float64 {
	return 0
}

func (e *ExchangeTrade) Name() string {
	return "exchange_test"
}

func (e *ExchangeTrade) Run(i ...interface{}) chan *Order {
	fmt.Println(append([]interface{}{"config:"}, i...)...)
	e.ch = make(chan *Order)
	return e.ch
}

func (e *ExchangeTrade) NewOrder(o *Order) (*Order, error) {
	o.Id = time.Now().Format(time.StampMilli)

	go func() {
		<-time.After(time.Millisecond * time.Duration(rand.Intn(1000-1)+1))
		o.Time = time.Now()
		e.ch <- o
	}()
	return o, nil
}

func TestTrades(t1 *testing.T) {
	t1.Run("trades", func(t1 *testing.T) {
		Reg(new(ExchangeTrade))

		_ = comp.Init(map[string]interface{}{
			"config": "./test/server.json",
		})

		go func() {
			<-time.After(time.Second)
			ch, _, _ := WatchOrder("exchange_test", "adausdt")
			for o := range ch {
				log.Info("新订单回复:", o)
			}
		}()

		go func() {
			<-time.After(time.Second)
			for {
				<-time.After(time.Millisecond * time.Duration(rand.Intn(1000-1)+1))

				ty := OrderType(rand.Intn(1))
				side := PositionSide(rand.Intn(1))
				price := 2.0 + rand.Float64()*(3.0-2.0)
				quantity := 0.1 + rand.Float64()*(3.0-0.1)

				ret, _ := PutOrder(NewOrder("exchange_test",
					"adausdt",
					ty,
					side,
					price,
					quantity,
				))
				log.Info("订单请求完成:", ret)
			}
		}()

		done := make(chan bool)
		go func() {
			<-time.After(time.Minute)
			done <- true
		}()

		_ = comp.Start(done)
	})
}
